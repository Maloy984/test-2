if (document.body.clientWidth < 1024) {
  const toggleClasses = () => {
    document.querySelector(".header").classList.toggle("btn-active");
    document.body.classList.toggle("body-scroll-off");
  };

  document
    .querySelector(".header__btn")
    .addEventListener("click", toggleClasses);


    var closeNav = document.querySelectorAll('.header__navigation');
    closeNav[0].addEventListener('click', toggleClasses);

  // document.querySelector(".header__btn").addEventListener("click", () => {
  //   document.querySelector(".header").classList.toggle("btn-active");
  //   document.body.classList.toggle("body-scroll-off");
  // });
}
